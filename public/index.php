<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SPS Documentation Generator API</title>
</head>
<body>

<h2>Stone Pillar Studios</h2>
<h1>Documentation Generator API</h1>

<h3>What is this?</h3>
<p>Call our endpoint with some specific JSON and it'll send back a neatly generated API documentation.</p>

<h3>URL</h3>
<p>POST https://docgen.stonepillarstudios.com/v1/generator</p>

<h3>Example JSON</h3>
<pre>
<?php
    echo file_get_contents("./test/example-corp.json");
?>
</pre>
<h3>Example Result</h3>
<p><a href="./test/example-corp.php">Example Corp</a></p>

</body>
</html>