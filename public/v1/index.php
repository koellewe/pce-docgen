<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 25 June 2018
 * Time: 2:36 PM
 */

require_once './vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

//$app->add(new RKA\Middleware\IpAddress());

$c = $app->getContainer();

//Override the default Not Found Handler
$c['notFoundHandler'] = function ($c) {
    return function (Request $request, Response $response) use ($c) {
        return $response
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('<h1>This is not the page you\'re looking for</h1>');
    };
};

//Override the default Not Allowed Handler
$c['notAllowedHandler'] = function ($c) {
    return function (Request $request, Response $response) use ($c) {
        return $response
            ->withStatus(405)
            ->withHeader('Content-Type', 'text/html')
            ->write('<h1>You\'re not supposed to GET here, idiot.</h1>');
    };
};

$c['settings']['displayErrorDetails'] = true;


$app->post('/generator', function(Request $req, Response $resp){

    $in = $req->getParsedBody();
    $ou = "<html><head><style>".file_get_contents('./documentation.css')."</style>";
    $status = 200;

    if (good_format($in)){

        $ou .= "<title>".$in['head']['title']."</title></head><body><br/>" .
            "<h1>".$in['head']['title']."</h1>" .
            "<table id='tbl_doc_details'>".
            "<tr><td>Version</td><td>".$in['head']['version']."</td></tr>".
            "<tr><td>Author</td><td>".$in['head']['author']."</td></tr>".
            "<tr><td>Last Updated</td><td>".$in['head']['last_updated']."</td></tr>".
            "</table><br/>".

            "<h2>Access</h2>".
            "<p>To access the API, use the following address:</p>".
            "<p class='code'>".$in['access']['url_form']."</p>";

        if (is_array($in['access']['url_info'])){
            $ou .= '<ul>';
            foreach ($in['access']['url_info'] as $uinfo){
                $ou .= '<li><span class="code">'.$uinfo['title'].'</span>: '.$uinfo['desc'].'</li>';
            }
            $ou .= '</ul>';
        }

        if (is_array($in['access']['custom_subs'])){
            foreach ($in['access']['custom_subs'] as $usubs){
                $ou .= '<h3>'.$usubs['title'].'</h3>'.
                    '<p>'.$usubs['desc'].'</p>';
            }
        }

        $ou .= "<br/><h2>Endpoints</h2>".
            "<h3>Index</h3>".
            "<ol>";

        foreach($in['endpoint_groups'] as $endpoint_group){
            $ou .= "<li>".$endpoint_group['title']."<ul>";

            foreach($endpoint_group['endpoints'] as $endpoint){
                $ou .= '<li><a href="#endpoint_'.safen_id($endpoint['method'].$endpoint['route']).'">'.
                    $endpoint['method'].' '.$endpoint['route'].'</a></li>';
            }

            $ou .= "</ul></li>";
        }

        $ou .="</ol><br/>";

        foreach ($in['endpoint_groups'] as $endpoint_group){

            $ou .= '<h3>'.$endpoint_group['title'].'</h3>';

            foreach($endpoint_group['endpoints'] as $endpoint){

                $ou .= '<h4 class="route_title" id="endpoint_'.safen_id($endpoint['method'].$endpoint['route']).'"
>'.$endpoint['method'].' '.$endpoint['route'].'</h4>'.
                    '<p>'.$endpoint['desc'].'</p>';

                $ou .= '<p class="mini_title">Request</p>'.
                    '<table class="json_desc">'.
                    "<tr>
                        <th>Field</th><th>Type</th><th>Description</th><th>Example</th>
                    </tr>";

                foreach($endpoint['request'] as $field){

                    $ou .= '<tr>'.
                        '<td>'.$field['field'].'</td><td>'.$field['type'].'</td><td>'.$field['desc'].'</td><td>'.$field['ex'].'</td>'.
                        '</tr>';

                }

                $ou .= '</table>'.

                    '<p class="mini_title">Response</p>'.
                    '<table class="json_desc">'.
                    "<tr>
                        <th>Field</th><th>Type</th><th>Description</th><th>Example</th>
                    </tr>";

                foreach($endpoint['response'] as $field){

                    $ou .= '<tr>'.
                        '<td>'.$field['field'].'</td><td>'.$field['type'].'</td><td>'.$field['desc'].'</td><td>'.$field['ex'].'</td>'.
                        '</tr>';

                }

                $ou .= '</table>';

                if (is_array($endpoint['success_codes'])){
                    $ou .= '<p class="mini_title">Success codes</p>'.
                        '<table class="codes_desc">'.
                        '<tr><th>Code</th><th>Description</th></tr>';

                    foreach($endpoint['success_codes'] as $success_code){
                        $ou .= '<tr><td>'.$success_code['code'].'</td><td>'.code_desc($success_code).'</td></tr>';
                    }

                    $ou .= '</table>';
                }

                if (is_array($endpoint['fail_codes'])){
                    $ou .= '<p class="mini_title">Failure codes</p>'.
                        '<table class="codes_desc">'.
                        '<tr><th>Code</th><th>Description</th></tr>';

                    foreach($endpoint['fail_codes'] as $fail_code){
                        $ou .= '<tr><td>'.$fail_code['code'].'</td><td>'.code_desc($fail_code).'</td></tr>';
                    }

                    $ou .= '</table>';
                }

                $ou .= '<br/>';

            }

            $ou .= '<br/><br/>';

        }

        $ou .= '<p>This documentation form was generated using <a href="https://docgen.stonepillarstudios.com">SPS Documentation Generator</a></p>';

    }else{

        $contents = json_encode($in);

        $ou .= "<title>Docgen 400</title></head><body><br/>
    <p><b>Failed to render documentation:</b> The raw JSON isn't in the correct format.</p>
    <p>Here's what we got:</p>
    <pre>".$contents."</pre>
</body>";
        $status = 400;
    }

    return $resp
        ->withStatus($status)
        ->withHeader("Content-Type", "text/html")
        ->write($ou);

});

function good_format(&$in){

    $sofar = isset($in['head']['title'], $in['head']['author'], $in['head']['version'], $in['head']['last_updated'],
        $in['access']['url_form'], $in['endpoint_groups']) and is_array($in['endpoint_groups']);

    return $sofar; //todo

}

function code_desc(array $status_code){

    if (isset($status_code['desc'])){
        return $status_code['desc'];
    }else{

        $http_status_codes = array(
            100 => 'Informational: Continue',
            101 => 'Informational: Switching Protocols',
            102 => 'Informational: Processing',
            200 => 'Successful: OK',
            201 => 'Successful: Created',
            202 => 'Successful: Accepted',
            203 => 'Successful: Non-Authoritative Information',
            204 => 'Successful: No Content',
            205 => 'Successful: Reset Content',
            206 => 'Successful: Partial Content',
            207 => 'Successful: Multi-Status',
            208 => 'Successful: Already Reported',
            226 => 'Successful: IM Used',
            300 => 'Redirection: Multiple Choices',
            301 => 'Redirection: Moved Permanently',
            302 => 'Redirection: Found',
            303 => 'Redirection: See Other',
            304 => 'Redirection: Not Modified',
            305 => 'Redirection: Use Proxy',
            306 => 'Redirection: Switch Proxy',
            307 => 'Redirection: Temporary Redirect',
            308 => 'Redirection: Permanent Redirect',
            400 => 'Client Error: Bad Request',
            401 => 'Client Error: Unauthorized',
            402 => 'Client Error: Payment Required',
            403 => 'Client Error: Forbidden',
            404 => 'Client Error: Not Found',
            405 => 'Client Error: Method Not Allowed',
            406 => 'Client Error: Not Acceptable',
            407 => 'Client Error: Proxy Authentication Required',
            408 => 'Client Error: Request Timeout',
            409 => 'Client Error: Conflict',
            410 => 'Client Error: Gone',
            411 => 'Client Error: Length Required',
            412 => 'Client Error: Precondition Failed',
            413 => 'Client Error: Request Entity Too Large',
            414 => 'Client Error: Request-URI Too Long',
            415 => 'Client Error: Unsupported Media Type',
            416 => 'Client Error: Requested Range Not Satisfiable',
            417 => 'Client Error: Expectation Failed',
            418 => 'Client Error: I\'m a teapot',
            419 => 'Client Error: Authentication Timeout',
            420 => 'Client Error: Enhance Your Calm',
            422 => 'Client Error: Unprocessable Entity',
            423 => 'Client Error: Locked',
            424 => 'Client Error: Failed Dependency',
            425 => 'Client Error: Unordered Collection',
            426 => 'Client Error: Upgrade Required',
            428 => 'Client Error: Precondition Required',
            429 => 'Client Error: Too Many Requests',
            431 => 'Client Error: Request Header Fields Too Large',
            444 => 'Client Error: No Response',
            449 => 'Client Error: Retry With',
            450 => 'Client Error: Blocked by Windows Parental Controls',
            451 => 'Client Error: Unavailable For Legal Reasons',
            494 => 'Client Error: Request Header Too Large',
            495 => 'Client Error: Cert Error',
            496 => 'Client Error: No Cert',
            497 => 'Client Error: HTTP to HTTPS',
            499 => 'Client Error: Client Closed Request',
            500 => 'Server Error: Internal Server Error',
            501 => 'Server Error: Not Implemented',
            502 => 'Server Error: Bad Gateway',
            503 => 'Server Error: Service Unavailable',
            504 => 'Server Error: Gateway Timeout',
            505 => 'Server Error: HTTP Version Not Supported',
            506 => 'Server Error: Variant Also Negotiates',
            507 => 'Server Error: Insufficient Storage',
            508 => 'Server Error: Loop Detected',
            509 => 'Server Error: Bandwidth Limit Exceeded',
            510 => 'Server Error: Not Extended',
            511 => 'Server Error: Network Authentication Required',
            598 => 'Server Error: Network read timeout error',
            599 => 'Server Error: Network connect timeout error',
        );

        return $http_status_codes[$status_code['code']];

    }

}

function safen_id($text){

    $safened = "";

    foreach (str_split($text) as $c){
        if (ctype_alpha($c)){
            $safened .= $c;
        }else if (in_array($c, array(' ', '/'))){
            $safened .= '_';
        }
    }

    return $safened;

}


try {
    $app->run();
}catch (\Slim\Exception\MethodNotAllowedException $e) {
    echo('<p>Slimphp threw an error. Check the logs</p>');
}catch (\Slim\Exception\NotFoundException $e) {
    echo('<p>Slimphp threw an error. Check the logs</p>');
}catch (\Exception $e){
    echo('<p>Slimphp threw an error. Check the logs</p>');
}